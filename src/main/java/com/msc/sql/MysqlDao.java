package com.msc.sql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;

public abstract class MysqlDao {
    private Logger logger= LoggerFactory.getLogger(this.getClass());
    private final String url;
    private final String username;
    private final String password;
    public MysqlDao(String url,String username,String password){
        this.url=url;
        this.username=username;
        this.password=password;
    }
    /**
     * Get item by specifying tag and its value.
     * */
    public abstract Object getItem(String tag,String value);
    /**
     * Add item to database.
     *
     * @return The id of last added item.
     * */
    public abstract int addItem(Object data);
    /**
     * Delete an item which has the specific tag and value.
     * */
    public abstract void deleteItem(String tag,String value);
    /**
     * Update an item matching the specific tag and value with the given data.
     * */
    public abstract void updateItem(String tag,String value,Object data);

    protected Object exec(DaoAction action){
        try{
            Connection c=DriverManager.getConnection(url,username,password);
            Object o= action.execute(c);
            c.close();
            return o;
        }catch (Exception e){
            logger.error("Dao error.",e);
            return null;
        }
    }

    public static Object binaryStreamToObject(InputStream stream) throws Exception{
        ObjectInputStream ois=new ObjectInputStream(stream);
        Object o= ois.readObject();
        ois.close();
        return o;
    }

    public static InputStream objectToBinaryStream(Object o) throws Exception{
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        ObjectOutputStream oos=new ObjectOutputStream(bos);
        oos.writeObject(o);
        return new ByteArrayInputStream(bos.toByteArray());
    }

    public static InputStream imageToStream(BufferedImage image) throws Exception{
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        ImageIO.write(image,"png",bos);
        return new ByteArrayInputStream(bos.toByteArray());
    }

    interface DaoAction{
        Object execute(Connection connection) throws Exception;
    }
}
