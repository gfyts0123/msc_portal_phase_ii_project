package com.msc.sql;

import com.msc.article.Article;
import com.msc.util.ALTransformer;
import com.msc.util.Util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ArticleDao extends MysqlDao{

    public static final String ID="id";

    public ArticleDao(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public Object getItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        return exec((c)->{
            Statement state=c.createStatement();
            ResultSet set;
            set=state.executeQuery(Util.formatWithoutQuote("SELECT * FROM articles WHERE `%s`=%s",tag,value));
            if(!set.next())
                return null;
            Article result=copyOf(set);
            set.close();
            state.close();
            return result;
        });
    }

    @Override
    public int addItem(Object data) {
        return (int)exec((c)->{
            Article detail=(Article)data;
            PreparedStatement state=c.prepareStatement(
                    Util.formatWithoutQuote("INSERT INTO articles(title,author,content,date,userlike,userdislike,guestlike,guestdislike,view,censor) VALUES ('%s',%d,'%s','%s',?,?,?,?,%d,%d)",
                            detail.title,
                            detail.authorId,
                            detail.content,
                            detail.date,
                            detail.views,
                            detail.censor),
                    Statement.RETURN_GENERATED_KEYS);
            state.setBlob(1,objectToBinaryStream(ALTransformer.toIntArray(detail.userLike)));
            state.setBlob(2,objectToBinaryStream(ALTransformer.toIntArray(detail.userDislike)));
            state.setBlob(3,objectToBinaryStream(detail.guestLike));
            state.setBlob(4,objectToBinaryStream(detail.guestDislike));
            state.execute();
            ResultSet set=state.getGeneratedKeys();
            set.next();
            int id=set.getInt(1);
            set.close();
            state.close();
            return id;
        });
    }

    @Override
    public void deleteItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            Statement state=c.createStatement();
            state.execute(Util.formatWithoutQuote("DELETE FROM articles WHERE `%s`=%s",tag,value));
            state.close();
            return null;
        });
    }

    @Override
    public void updateItem(String tag, String value, Object data) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            Article detail=(Article)data;
            PreparedStatement state=c.prepareStatement(Util.formatWithoutQuote("UPDATE articles SET title='%s',author=%d,content='%s',date='%s',userlike=?,userdislike=?,guestlike=?,guestdislike=?,view=%d,censor=%d WHERE `id`=%d",
                    detail.title,
                    detail.authorId,
                    detail.content,
                    detail.date,
                    detail.views,
                    detail.censor,
                    detail.id));
            state.setBlob(1,objectToBinaryStream(ALTransformer.toIntArray(detail.userLike)));
            state.setBlob(2,objectToBinaryStream(ALTransformer.toIntArray(detail.userDislike)));
            state.setBlob(3,objectToBinaryStream(detail.guestLike));
            state.setBlob(4,objectToBinaryStream(detail.guestDislike));
            state.execute();
            state.close();
            return null;
        });
    }

    public int[] getUncensoredList(){
        return (int[])exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT id FROM articles WHERE censor="+Article.NOT_CENSORED);
            ArrayList<Integer> result=new ArrayList<>();
            while(set.next())
                result.add(set.getInt("id"));
            set.close();
            state.close();
            return ALTransformer.toIntArray(result);
        });
    }
    /**
     * Get the article list for specified user.
     *
     * @see com.msc.sql.ArticleDao#getCountFor(int, boolean)
     * */
    public Article[] getListFor(int user,int off,int len,boolean displayUncensored){
        return (Article[])exec((c)->{
           Statement state=c.createStatement();
           ResultSet set=state.executeQuery("SELECT * FROM articles WHERE author="+user+" ORDER BY UNIX_TIMESTAMP(date) DESC"+(displayUncensored?";":" AND censor="+Article.CENSOR_PASS+";"));
           Article[] list=new Article[len];
           for(int i=0;i<off;i++)
               set.next();
           for(int i=0;i<len;i++){
               set.next();
               list[i]=copyOf(set);
           }
           set.close();
           state.close();
           return list;
        });
    }
    /**
     * Get the article list for search query.
     *
     * @see com.msc.sql.ArticleDao#getListFor(String, int, int, boolean)
     * */
    public Article[] getListFor(String query,int off,int len,boolean displayUncensored){
        return (Article[])exec((c)->{
            Statement state=c.createStatement();
            ResultSet set =state.executeQuery("SELECT * FROM articles WHERE title LIKE '%"+query+"%'"+(displayUncensored?";":" AND censor="+Article.CENSOR_PASS+";"));
            Article[] result=new Article[len];
            for(int i=0;i<off;i++)
                set.next();
            for(int i=0;i<len;i++){
                set.next();
                result[i]=copyOf(set);
            }
            return result;
        });
    }
    /**
     * Get all article list.
     * */
    public Article[] getListFor(int off,int len,boolean displayUncensored){
        return (Article[])exec((c)->{
            Statement state=c.createStatement();
            ResultSet set =state.executeQuery("SELECT * FROM articles"+(displayUncensored?"":" WHERE censor="+Article.CENSOR_PASS)+" ORDER BY UNIX_TIMESTAMP(date) DESC;");
            Article[] result=new Article[len];
            for(int i=0;i<off;i++)
                set.next();
            for(int i=0;i<len;i++){
                set.next();
                result[i]=copyOf(set);
            }
            return result;
        });
    }
    public int getCountFor(boolean displayUncensored){
        return (int)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT COUNT(*) FROM articles"+(displayUncensored?"":" WHERE censor="+Article.CENSOR_PASS)+";");
            set.next();
            return set.getInt(1);
        });
    }
    /**
     * Get the article list length for specified user.
     *
     * @see com.msc.sql.ArticleDao#getListFor(int, int, int, boolean)
     * */
    public int getCountFor(int user,boolean displayUncensored){
        return (int)exec((c)->{
           Statement state=c.createStatement();
           ResultSet set=state.executeQuery("SELECT COUNT(*) FROM articles WHERE author="+user+(displayUncensored?";":" AND censor="+Article.CENSOR_PASS+";"));
           set.next();
           return set.getInt(1);
        });
    }
    /**
     * Get the article list length for search query.
     *
     * @see com.msc.sql.ArticleDao#getListFor(String, int, int, boolean)
     * */
    public int getCountFor(String query,boolean displayUncensored){
        return (int)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT COUNT(*) FROM articles WHERE title LIKE '%"+query+"%'"+(displayUncensored?";":" AND censor="+Article.CENSOR_PASS+";"));
            set.next();
            return set.getInt(1);
        });
    }
    @SuppressWarnings("unchecked")
    private Article copyOf(ResultSet set)throws Exception {
        Article result=new Article(set.getString("title"),set.getInt("author"),set.getString("content"));
        result.censor=set.getInt("censor");
        result.id=set.getInt("id");
        result.userLike=ALTransformer.toIntegerList((int[])binaryStreamToObject(set.getBinaryStream("userlike")));
        result.userDislike=ALTransformer.toIntegerList((int[])binaryStreamToObject(set.getBinaryStream("userdislike")));
        result.guestLike=(ArrayList<String>)binaryStreamToObject(set.getBinaryStream("guestlike"));
        result.guestDislike=(ArrayList<String>) binaryStreamToObject(set.getBinaryStream("guestdislike"));
        result.date=set.getString("date");
        result.views=set.getInt("view");
        return result;
    }
}
