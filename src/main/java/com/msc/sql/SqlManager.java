package com.msc.sql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class SqlManager {
    private final String url;
    private final String username;
    private final String password;
    private static final Logger logger = LoggerFactory.getLogger(SqlManager.class);
    public UserDao user;
    public ArticleDao article;
    public CommentDao comment;
    public PictureDao picture;
    public SqlManager(String url,String username,String password){
        this.url=url;
        this.username=username;
        this.password=password;
    }
    /**
     * 初始化数据库，创建必要的数据表和Dao。
     * */
    public void initialize(){
        try {
            Connection con=DriverManager.getConnection(url,username,password);
            Statement state=con.createStatement();
            state.execute("CREATE TABLE IF NOT EXISTS users(`id` INT NOT NULL AUTO_INCREMENT,`username` VARCHAR(20) NOT NULL,`password` VARCHAR(100) NOT NULL,`email` VARCHAR(50) NOT NULL,`gender` TINYINT NOT NULL,`admin` TINYINT NOT NULL,`desc` VARCHAR(200) NOT NULL,`avatar` MEDIUMBLOB,PRIMARY KEY (`id`),UNIQUE KEY(`username`))ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            state.execute("CREATE TABLE IF NOT EXISTS articles(`id` INT NOT NULL AUTO_INCREMENT,`title` VARCHAR(50) NOT NULL,`author` INT NOT NULL,`content` MEDIUMTEXT NOT NULL,`date` DATETIME NOT NULL,`userlike` BLOB NOT NULL,`guestlike` BLOB NOT NULL,`userdislike` BLOB NOT NULL,`guestdislike` BLOB NOT NULL,`view` INT NOT NULL,`censor` TINYINT NOT NULL,PRIMARY KEY (`id`))ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            state.execute("CREATE TABLE IF NOT EXISTS comments(`id` INT NOT NULL AUTO_INCREMENT,`user` INT NOT NULL,`article` INT NOT NULL,`censor` TINYINT NOT NULL,`date` DATETIME NOT NULL,`content` TEXT NOT NULL,PRIMARY KEY (`id`),KEY (`article`))ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            state.execute("CREATE TABLE IF NOT EXISTS picture(`id` INT NOT NULL AUTO_INCREMENT,`user` INT NOT NULL,`picture` MEDIUMBLOB NOT NULL, PRIMARY KEY (`id`))ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            state.close();
            con.close();
        }catch (Exception e){
            logger.error("Failed to initialize database",e);
        }
        user=new UserDao(url,username,password);
        article=new ArticleDao(url,username,password);
        comment=new CommentDao(url,username,password);
        picture=new PictureDao(url,username,password);
    }
}
