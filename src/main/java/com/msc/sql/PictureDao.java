package com.msc.sql;

import com.msc.article.Picture;
import sun.awt.image.ByteArrayImageSource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class PictureDao extends MysqlDao{
    public static final String ID="id";
    public static final int PICTURE_NOT_EXIST=-1;
    public static final int NO_PERMISSION=1;
    public static final int PERMISSION_OK=0;
    public PictureDao(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public Object getItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        return exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT `picture`,`user` from picture WHERE `id`="+value);
            if(!set.next())
                return null;
            BufferedImage bi= ImageIO.read(set.getBinaryStream("picture"));
            int user=set.getInt("user");
            set.close();
            state.close();
            return new Picture(bi,user);
        });
    }

    @Override
    public int addItem(Object data) {
        return (int)exec((c)->{
            Picture p=(Picture)data;
            PreparedStatement state=c.prepareStatement("INSERT INTO picture(`picture`,`user`) VALUES (?,"+p.user+")",Statement.RETURN_GENERATED_KEYS);
            ByteArrayOutputStream bos=new ByteArrayOutputStream();
            ImageIO.write(p.image,"png",bos);
            ByteArrayInputStream bis=new ByteArrayInputStream(bos.toByteArray());
            state.setBlob(1,bis);
            state.execute();
            ResultSet set=state.getGeneratedKeys();
            set.next();
            int i= set.getInt(1);
            set.close();
            state.close();
            return i;
        });
    }

    @Override
    public void deleteItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            Statement state=c.createStatement();
            state.execute("DELETE FROM picture WHERE `id`="+value);
            state.close();
            return null;
        });
    }

    public int checkPermission(int user,int picture){
        return (int)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT `user` FROM picture WHERE `id`="+picture);
            if(!set.next())
                return PICTURE_NOT_EXIST;
            if(set.getInt("user")==user)
                return PERMISSION_OK;
            else
                return NO_PERMISSION;
        });
    }
    @Override
    @Deprecated
    public void updateItem(String tag, String value, Object data) {
        throw new UnsupportedOperationException("Picture database does not support update operation.");
    }
}
