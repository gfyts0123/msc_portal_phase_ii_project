package com.msc.sql;

import com.msc.article.Comment;
import com.msc.util.ALTransformer;
import com.msc.util.Util;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CommentDao extends MysqlDao{

    public static final String ID="id";

    public CommentDao(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public Object getItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        return exec((c)->{
            Statement state=c.createStatement();
            ResultSet set;
            set=state.executeQuery(Util.formatWithoutQuote("SELECT * FROM comments WHERE `%s`=%s",tag,value));
            if(!set.next())
                return null;
            Comment result=new Comment(set.getInt("article"),set.getInt("user"),set.getString("content"));
            result.id=set.getInt("id");
            result.censor=set.getInt("censor");
            result.date=set.getString("date");
            state.close();
            return result;
        });
    }

    @Override
    public int addItem(Object data) {
        return (int)exec((c)->{
            Comment detail=(Comment)data;
            Statement state=c.createStatement();
            state.execute(
                    Util.formatWithoutQuote("INSERT INTO comments(article,user,content,date,censor) VALUES (%d,%d,'%s','%s',%d)",
                            detail.article,
                            detail.user,
                            detail.content,
                            detail.date,
                            detail.censor),
                    Statement.RETURN_GENERATED_KEYS);
            ResultSet set=state.getGeneratedKeys();
            set.next();
            int id=set.getInt(1);
            state.close();
            return id;
        });
    }

    @Override
    public void deleteItem(String tag, String value) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            Statement state=c.createStatement();
            state.execute(Util.formatWithoutQuote("DELETE FROM comments WHERE `%s`=%s",tag,value));
            return null;
        });
    }

    @Override
    public void updateItem(String tag, String value, Object data) {
        if(!tag.equals(ID))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            Comment detail=(Comment)data;
            Statement state=c.createStatement();
            state.execute(Util.formatWithoutQuote("UPDATE comments SET article=%d,user=%d,content='%s',date='%s',censor=%d WHERE `id`=%d",
                    detail.article,
                    detail.user,
                    detail.content,
                    detail.date,
                    detail.censor,
                    detail.id));
            state.close();
            return null;
        });
    }

    public int getCensored(int id){
        return (int)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery(Util.formatWithoutQuote("SELECT censor FROM comments WHERE `id`=%d",id));
            if(!set.next())
                return -1;
            return set.getInt("censor");
        });
    }

    public int[] getUncensoredList(){
        return (int[])exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT id FROM comments WHERE censor="+ Comment.NOT_CENSORED);
            ArrayList<Integer> result=new ArrayList<>();
            while(set.next())
                result.add(set.getInt("id"));
            return ALTransformer.toIntArray(result);
        });
    }

    public Comment[] getListFor(int article,int off,int len,boolean admin,int user){
        return (Comment[])exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT * FROM comments WHERE article="+article
                                            +(admin?"":" AND (censor="+Comment.CENSOR_PASS+" OR user="+user+")")+" ORDER BY UNIX_TIMESTAMP(date);");
            Comment[] result=new Comment[len];
            for(int i=0;i<off;i++)
                set.next();
            for(int i=0;i<len;i++){
                set.next();
                Comment co=new Comment(article,set.getInt("user"),set.getString("content"));
                co.date=set.getString("date");
                co.id=set.getInt("id");
                co.censor=set.getInt("censor");
                result[i]=co;
            }
            return result;
        });
    }

    public int getCountFor(int article,boolean admin,int user){
        return (int)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT COUNT(*) FROM comments WHERE article="+article
                                        +(admin?"":" AND (censor="+Comment.CENSOR_PASS+" OR user="+user+")"));
            set.next();
            return set.getInt(1);
        });
    }
}