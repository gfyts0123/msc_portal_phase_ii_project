package com.msc.sql;

import com.msc.SharedData;
import com.msc.user.MyUserDetails;
import com.msc.util.Util;

import javax.imageio.ImageIO;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserDao extends MysqlDao{

    public static final String USERNAME="username";
    public static final String ID="id";

    public UserDao(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public Object getItem(String tag, String value) {
        if(!(tag.equals(USERNAME)||tag.equals(ID)))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        return exec((c)->{
            Statement state=c.createStatement();
            ResultSet set;
            if(tag.equals(USERNAME))
                set=state.executeQuery(Util.formatWithoutQuote("SELECT * FROM users WHERE `%s`='%s';",tag,value));
            else
                set=state.executeQuery(Util.formatWithoutQuote("SELECT * FROM users WHERE `%s`=%s;",tag,value));
            if(!set.next())
                return null;
            MyUserDetails result=new MyUserDetails(set.getString("username"),set.getString("password"),set.getString("email"),set.getInt("admin"),true);
            result.gender=set.getInt("gender");
            result.description=set.getString("desc");
            result.id=set.getInt("id");
            InputStream avatar=set.getBinaryStream("avatar");
            if(set.wasNull())
                result.avatar= SharedData.defaultAvatar;
            else
                result.avatar= ImageIO.read(avatar);
            state.close();
            return result;
        });
    }

    @Override
    public int addItem(Object data) {
        return (int)exec((c)->{
            MyUserDetails detail=(MyUserDetails)data;
            PreparedStatement state;
            if (detail.avatar != null) {
                state = c.prepareStatement(Util.formatWithoutQuote(
                        "INSERT INTO users(username,password,email,gender,`desc`,admin,avatar) " +
                        "VALUES ('%s','%s','%s',%d,'%s',%d,?);",
                        detail.getUsername(),
                        detail.getPassword(),
                        detail.email,
                        detail.gender,
                        detail.description,
                        detail.getAdmin()),Statement.RETURN_GENERATED_KEYS);
                state.setBlob(1,imageToStream(detail.avatar));
            }else{
                state = c.prepareStatement(Util.formatWithoutQuote(
                        "INSERT INTO users(username,password,email,gender,`desc`,admin) " +
                        "VALUES ('%s','%s','%s',%d,'%s',%d);",
                        detail.getUsername(),
                        detail.getPassword(),
                        detail.email,
                        detail.gender,
                        detail.description,
                        detail.getAdmin()),Statement.RETURN_GENERATED_KEYS);
            }
            state.execute();
            ResultSet set=state.getGeneratedKeys();
            set.next();
            int id=set.getInt(1);
            state.close();
            return id;
        });
    }

    @Override
    public void deleteItem(String tag, String value) {
        if(!(tag.equals(USERNAME)||tag.equals(ID)))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
           Statement state=c.createStatement();
           if(tag.equals(USERNAME))
               state.execute(Util.formatWithoutQuote("DELETE FROM users WHERE `%s`='%s';",tag,value));
           else
               state.execute(Util.formatWithoutQuote("DELETE FROM users WHERE `%s`=%s;",tag,value));
           return null;
        });
    }

    @Override
    public void updateItem(String tag, String value, Object data) {
        if(!(tag.equals(USERNAME)||tag.equals(ID)))
            throw new IllegalArgumentException("Invalid index tag:"+tag);
        exec((c)->{
            MyUserDetails detail=(MyUserDetails)data;
            PreparedStatement state;
            if(detail.avatar!=null) {
                state = c.prepareStatement(Util.formatWithoutQuote(
                        "UPDATE users SET username='%s',password='%s',email='%s',gender=%d,admin=%d,`desc`='%s',avatar=? " +
                                "WHERE `%s`="+(tag.equals(USERNAME)?"'%s';":"%s;"),
                        detail.getUsername(),
                        detail.getPassword(),
                        detail.email,
                        detail.gender,
                        detail.getAdmin(),
                        detail.description,
                        tag,
                        value));
                state.setBlob(1,imageToStream(detail.avatar));
            }else {
                state = c.prepareStatement(Util.formatWithoutQuote(
                        "UPDATE users SET username='%s',password='%s',email='%s',gender=%d,`desc`='%s' " +
                                "WHERE `%s`="+(tag.equals(USERNAME)?"'%s';":"%s;"),
                        detail.getUsername(),
                        detail.getPassword(),
                        detail.email,
                        detail.gender,
                        detail.description,
                        tag,
                        value));
            }
            state.execute();
            state.close();
            return null;
        });
    }
    public String getUsername(int id){
        return (String)exec((c)->{
            Statement state=c.createStatement();
            ResultSet set=state.executeQuery("SELECT username FROM users WHERE id="+id);
            if(!set.next())
                return null;
            else
                return set.getString(1);
        });
    }
}
