package com.msc.article;

import com.msc.SharedData;
import com.msc.sql.ArticleDao;
import com.msc.sql.PictureDao;
import com.msc.sql.UserDao;
import com.msc.user.MyUserDetails;
import com.msc.util.ControllerUtil;
import com.msc.util.Util;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@RestController
public class ArticleRestController {

    @PostMapping("/article/new")
    public String newArticle(@RequestParam("title")String title,
                             @RequestParam("content")String content){
        if(title.equalsIgnoreCase(""))
            return Util.formatURI(SharedData.operation_fail,"No title provided.");
        if(title.length()>50)
            return Util.formatURI(SharedData.operation_fail,"Title too long.");
        if(content.getBytes(StandardCharsets.UTF_8).length>64*1024)
            return Util.formatURI(SharedData.operation_fail,"Content too long");
        MyUserDetails user=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Article article=new Article(title,user.id,content);
        SharedData.sqlManager.article.addItem(article);
        return SharedData.operation_success_no_information;
    }

    @PostMapping("/article/modify")
    public String modifyArticle(@RequestParam("title")String title,
                                @RequestParam("content")String content,
                                @RequestParam("id")int id){
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(article==null)
            return Util.formatURI(SharedData.operation_fail,"Article not exist");
        if(!ControllerUtil.hasEditPerm(article.authorId))
            return Util.formatURI(SharedData.operation_fail,"Access denied");
        if(title.equalsIgnoreCase(""))
            return Util.formatURI(SharedData.operation_fail,"No title provided");
        if(title.length()>50)
            return Util.formatURI(SharedData.operation_fail,"Title too long");
        if(content.getBytes(StandardCharsets.UTF_8).length>64*1024)
            return Util.formatURI(SharedData.operation_fail,"Content too long");
        article.title=title;
        article.content=content;
        article.censor=Article.NOT_CENSORED;
        SharedData.sqlManager.article.updateItem(ArticleDao.ID,Integer.toString(id),article);
        return SharedData.operation_success_no_information;
    }

    @PostMapping("/article/getlist")
    public String getArticleList(@RequestParam("username")String username,
                                 @RequestParam(value = "off",required = false,defaultValue = "0")int off,
                                 @RequestParam(value = "length",required = false,defaultValue = "0")int length){
        MyUserDetails user=(MyUserDetails) SharedData.sqlManager.user.getItem(UserDao.USERNAME,username);
        if(user==null)
            return Util.formatURI(SharedData.operation_fail,"User not exist");
        StringBuilder sb=new StringBuilder("{\"state\":\"success\",\"listlength\":");
        boolean editPerm=ControllerUtil.hasEditPerm(user.id);
        int size=SharedData.sqlManager.article.getCountFor(user.id,editPerm);
        sb.append(size).append(",\"list\":[");
        if(length<0||off<0||off+length>size)
            return Util.formatURI(SharedData.operation_fail,"Index out of bounds");
        if(length>20)
            return Util.formatURI(SharedData.operation_fail,"Length too large");
        Article[] array=SharedData.sqlManager.article.getListFor(user.id,off,length,editPerm);
        if(length!=0) {
            for(int i=0;i<length;i++) {
                Article a=array[i];
                sb.append(Util.formatURI(SharedData.article_prompt_template,
                        a.title,
                        a.views,
                        a.userLike.size() + a.guestLike.size(),
                        a.userDislike.size() + a.guestDislike.size(),
                        username,
                        a.id,
                        a.date,
                        a.censor,
                        SharedData.sqlManager.comment.getCountFor(a.id,ControllerUtil.isAdmin(),user.id)))
                        .append(',');
            }
            sb.deleteCharAt(sb.length()-1);
        }
        return sb.append("]}").toString();
    }

    @PostMapping("/article/fetch")
    public String getArticle(@RequestParam("id")int id, HttpServletRequest request){
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(article==null)
            return Util.formatURI(SharedData.operation_fail,"Article not exist");
        MyUserDetails author=(MyUserDetails)SharedData.sqlManager.user.getItem(UserDao.ID,Integer.toString(article.authorId));
        if(!ControllerUtil.hasEditPerm(author.id)&&article.censor!=Article.CENSOR_PASS)
            return Util.formatURI(SharedData.operation_fail,"Article not censored");
        int express;
        if(ControllerUtil.isAuthenticated()){
            MyUserDetails current=(MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            express=article.userLike.contains(current.id)?1:
                    article.userDislike.contains(current.id)?2:0;
        }else{
            String ip=ControllerUtil.getClientIP(request);
            express=article.guestLike.contains(ip)?1:
                    article.guestDislike.contains(ip)?2:0;
        }
        article.views++;
        SharedData.sqlManager.article.updateItem(ArticleDao.ID,Integer.toString(id),article);
        return "{\"state\":\"success\",\"article\":"+Util.formatURI(
                SharedData.article_info_template,
                article.title,
                article.content,
                article.views,
                article.userLike.size()+article.guestLike.size(),
                article.userDislike.size()+article.guestDislike.size(),
                author.getUsername(),
                article.id,
                express,
                article.date,
                article.censor)+"}";
    }
    @PostMapping("/article/delete")
    public String deleteArticle(@RequestParam("id")int id){
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(article==null)
            return Util.formatURI(SharedData.operation_fail,"Article not exist");
        if(!ControllerUtil.hasEditPerm(article.authorId))
            return Util.formatURI(SharedData.operation_fail,"Access denied");
        SharedData.sqlManager.article.deleteItem(ArticleDao.ID,Integer.toString(id));
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/article/express")
    public String express(@RequestParam("id")int id,
                          @RequestParam("action")int action,
                          HttpServletRequest request){
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(article==null)
            return Util.formatURI(SharedData.operation_fail,"Article not exist");
        if(article.censor!=Article.CENSOR_PASS)
            return Util.formatURI(SharedData.operation_fail,"Article not censored");
        if(action<0||action>2)
            return Util.formatURI(SharedData.operation_fail,"Invalid action");
        int express;
        MyUserDetails current=null;
        String ip=null;
        if(ControllerUtil.isAuthenticated()){
            current=(MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            express=article.userLike.contains(current.id)?1:
                    article.userDislike.contains(current.id)?2:0;
        }else{
            ip=ControllerUtil.getClientIP(request);
            express=article.guestLike.contains(ip)?1:
                    article.guestDislike.contains(ip)?2:0;
        }
        if(express==0&&action==0)
            return Util.formatURI(SharedData.operation_fail,"Not like/disliked");
        if((express==1&&action==1)||(express==2&&action==2))
            return Util.formatURI(SharedData.operation_fail,"Already like/disliked");
        if(ControllerUtil.isAuthenticated()){
            assert current!=null;
            switch(action){
                case 0:
                    article.userLike.remove((Object)current.id);
                    article.userDislike.remove((Object)current.id);
                    break;
                case 1:
                    article.userDislike.remove((Object)current.id);
                    article.userLike.add(current.id);
                    break;
                case 2:
                    article.userLike.remove((Object)current.id);
                    article.userDislike.add(current.id);
                    break;
            }
        }else{
            assert ip!=null;
            switch(action){
                case 0:
                    article.guestLike.remove(ip);
                    article.guestDislike.remove(ip);
                    break;
                case 1:
                    article.guestDislike.remove(ip);
                    article.guestLike.add(ip);
                    break;
                case 2:
                    article.guestLike.remove(ip);
                    article.guestDislike.add(ip);
                    break;
            }
        }
        SharedData.sqlManager.article.updateItem(ArticleDao.ID,Integer.toString(article.id),article);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/article/uncensored")
    public String getUncensoredList(){
        return "{\"state\":\"success\",\"list\":"+ Arrays.toString(SharedData.sqlManager.article.getUncensoredList())+"}";
    }
    @PostMapping("/article/censor")
    public String censorArticle(@RequestParam("id")int id,
                                @RequestParam("censor")int censor){
        if(censor<0||censor>2)
            return Util.formatURI(SharedData.operation_fail,"Invalid censor value");
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(article==null)
            return Util.formatURI(SharedData.operation_fail,"Article not exist");
        article.censor=censor;
        SharedData.sqlManager.article.updateItem(ArticleDao.ID,Integer.toString(article.id),article);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/article/search")
    public String searchArticle(@RequestParam("query")String query,
                                @RequestParam(value = "off",required = false,defaultValue = "0")int off,
                                @RequestParam(value = "length",required = false,defaultValue = "0")int length){
        if(query.equalsIgnoreCase(""))
            return Util.formatURI(SharedData.operation_fail,"No query provided");
        int size=SharedData.sqlManager.article.getCountFor(query,false);
        StringBuilder sb=new StringBuilder("{\"state\":\"success\",\"listlength\":");
        sb.append(size).append(",\"list\":[");
        if(off<0||length<0||off+length>size)
            return Util.formatURI(SharedData.operation_fail,"Index out of bounds");
        if(length>20)
            return Util.formatURI(SharedData.operation_fail,"Length too large");
        Article[] array=SharedData.sqlManager.article.getListFor(query,off,length,false);
        if(length!=0) {
            for(int i=off;i<off+length;i++) {
                Article a=array[i];
                sb.append(Util.formatURI(SharedData.article_prompt_template,
                        a.title,
                        a.views,
                        a.userLike.size() + a.guestLike.size(),
                        a.userDislike.size() + a.guestDislike.size(),
                        SharedData.sqlManager.user.getUsername(a.authorId),
                        a.id,
                        a.date,
                        a.censor,
                        SharedData.sqlManager.comment.getCountFor(a.id,ControllerUtil.isAdmin(),ControllerUtil.getCurrentUserId())))
                        .append(',');
            }
            sb.deleteCharAt(sb.length()-1);
        }
        return sb.append("]}").toString();
    }

    @PostMapping("/article/feed")
    public String getFeed(@RequestParam(value = "off",required = false,defaultValue = "0")int off,
                          @RequestParam(value = "length",required = false,defaultValue = "0")int length){
        int size=SharedData.sqlManager.article.getCountFor(false);
        StringBuilder sb=new StringBuilder("{\"state\":\"success\",\"listlength\":");
        sb.append(size).append(",\"list\":[");
        if(off<0||length<0||off+length>size)
            return Util.formatURI(SharedData.operation_fail,"Index out of bounds");
        if(length>20)
            return Util.formatURI(SharedData.operation_fail,"Length too large");
        Article[] array=SharedData.sqlManager.article.getListFor(off,length,false);
        if(length!=0) {
            for(int i=off;i<off+length;i++) {
                Article a=array[i];
                sb.append(Util.formatURI(SharedData.article_prompt_template,
                        a.title,
                        a.views,
                        a.userLike.size() + a.guestLike.size(),
                        a.userDislike.size() + a.guestDislike.size(),
                        SharedData.sqlManager.user.getUsername(a.authorId),
                        a.id,
                        a.date,
                        a.censor,
                        SharedData.sqlManager.comment.getCountFor(a.id,ControllerUtil.isAdmin(),ControllerUtil.getCurrentUserId())))
                        .append(',');
            }
            sb.deleteCharAt(sb.length()-1);
        }
        return sb.append("]}").toString();
    }
    @PostMapping("/article/image/upload")
    public String uploadImage(@RequestParam("image")MultipartFile image){
        if(image.getSize()>3*1024*1024)
            return Util.formatURI(SharedData.operation_fail,"Image too large");
        try {
            BufferedImage bi= ImageIO.read(image.getInputStream());
            MyUserDetails user=(MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            int id=SharedData.sqlManager.picture.addItem(new Picture(bi,user.id));
            return "{\"state\":\"success\",\"id\":"+id+"}";
        }catch (Exception e){
            return Util.formatURI(SharedData.operation_fail,"Failed to parse image");
        }
    }
    @PostMapping("/article/image/delete")
    public String deleteImage(@RequestParam("id")int id){
        MyUserDetails user=(MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int result=SharedData.sqlManager.picture.checkPermission(user.id,id);
        if(result==PictureDao.PICTURE_NOT_EXIST)
            return Util.formatURI(SharedData.operation_fail,"Picture not exist");
        else if(result==PictureDao.NO_PERMISSION&&user.getAdmin()!=MyUserDetails.ADMIN)
            return Util.formatURI(SharedData.operation_fail,"Access denied");
        SharedData.sqlManager.picture.deleteItem(PictureDao.ID,Integer.toString(id));
        return SharedData.operation_success_no_information;
    }
    @GetMapping("/article/image")
    public byte[] getImage(@RequestParam("id")int id, HttpServletResponse response){
        Picture p=(Picture) SharedData.sqlManager.picture.getItem(PictureDao.ID,Integer.toString(id));
        response.setContentType("image/png");
        BufferedImage target;
        if(p==null)
            target=SharedData.picture404;
        else
            target=p.image;
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        try{
            ImageIO.write(target,"png",bos);
            return bos.toByteArray();
        }catch (Exception e){
            return "Internal server error".getBytes();
        }
    }
}
