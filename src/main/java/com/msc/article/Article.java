package com.msc.article;

import com.msc.SharedData;

import java.util.ArrayList;
import java.util.Calendar;

public class Article {
    public int id;
    public String title;
    public final int authorId;
    public String content;
    public String date;
    public ArrayList<Integer> userLike=new ArrayList<>();
    public ArrayList<String> guestLike=new ArrayList<>();
    public ArrayList<Integer> userDislike=new ArrayList<>();
    public ArrayList<String> guestDislike=new ArrayList<>();
    public int views=0;
    public int censor=NOT_CENSORED;

    public static final int NOT_CENSORED=0;
    public static final int CENSOR_FAIL=1;
    public static final int CENSOR_PASS=2;
    public Article(String title,int authorId,String content){
        this.title=title;
        this.authorId=authorId;
        this.content=content;
        date= SharedData.dateFormat.format(Calendar.getInstance().getTime());
    }
}
