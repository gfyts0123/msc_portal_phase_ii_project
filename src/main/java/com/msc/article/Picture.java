package com.msc.article;

import java.awt.image.BufferedImage;

public class Picture {
    public final BufferedImage image;
    public final int user;
    public Picture(BufferedImage image,int user){
        this.image=image;
        this.user=user;
    }
}
