package com.msc.article;

import com.msc.SharedData;
import com.msc.sql.ArticleDao;
import com.msc.sql.CommentDao;
import com.msc.sql.UserDao;
import com.msc.user.MyUserDetails;
import com.msc.util.ControllerUtil;
import com.msc.util.Util;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class CommentRestController {

    @PostMapping("/comment/new")
    public String commentArticle(@RequestParam("id")int id,
                                 @RequestParam("content")String content){
        Article article= (Article) SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(id!=-1) {
            if (article == null)
                return Util.formatURI(SharedData.operation_fail, "Article not exist");
            if(article.censor!=Article.CENSOR_PASS)
                return Util.formatURI(SharedData.operation_fail,"Article not censored");
        }
        int user=Comment.GUEST_USER;
        if(ControllerUtil.isAuthenticated())
            user= ((MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).id;
        Comment comment=new Comment(id,user,content);
        SharedData.sqlManager.comment.addItem(comment);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/comment/edit")
    public String editComment(@RequestParam("id")int id,
                              @RequestParam("content")String content){
        Comment comment=(Comment)SharedData.sqlManager.comment.getItem(CommentDao.ID,Integer.toString((id)));
        if(comment==null)
            return Util.formatURI(SharedData.operation_fail,"Comment not exist");
        if(!ControllerUtil.hasEditPerm(comment.user))
            return Util.formatURI(SharedData.operation_fail,"Access denied");
        comment.content=content;
        comment.censor=Comment.NOT_CENSORED;
        SharedData.sqlManager.comment.updateItem(CommentDao.ID,Integer.toString(id),comment);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/comment/delete")
    public String deleteComment(@RequestParam("id")int id){
        Comment comment=(Comment)SharedData.sqlManager.comment.getItem(CommentDao.ID,Integer.toString((id)));
        if(comment==null)
            return Util.formatURI(SharedData.operation_fail,"Comment not exist");
        if(!ControllerUtil.hasEditPerm(comment.user))
            return Util.formatURI(SharedData.operation_fail,"Access denied");
        SharedData.sqlManager.comment.deleteItem(CommentDao.ID,Integer.toString(id));
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/comment/getlist")
    public String getCommentList(@RequestParam("id")int id,
                                 @RequestParam(value = "off",required = false,defaultValue = "0")int off,
                                 @RequestParam(value = "length",required = false,defaultValue = "0")int length){
        Article article=(Article)SharedData.sqlManager.article.getItem(ArticleDao.ID,Integer.toString(id));
        if(id!=-1) {
            if (article == null)
                return Util.formatURI(SharedData.operation_fail, "Article not exist");
            if (article.censor != Article.CENSOR_PASS)
                return Util.formatURI(SharedData.operation_fail, "Article not censored");
        }
        StringBuilder sb=new StringBuilder("{\"state\":\"success\",\"listlength\":");
        int size=SharedData.sqlManager.comment.getCountFor(id,ControllerUtil.isAdmin(),ControllerUtil.getCurrentUserId());
        sb.append(size).append(",\"list\":[");
        if(off<0||length<0||off+length>size)
            return Util.formatURI(SharedData.operation_fail,"Index out of bounds");
        Comment[] list=SharedData.sqlManager.comment.getListFor(id,off,length, ControllerUtil.isAdmin(),ControllerUtil.getCurrentUserId());
        if(length!=0) {
            boolean guest;
            for (int i=0;i<length;i++) {
                Comment c=list[i];
                guest = c.user == Comment.GUEST_USER;
                sb.append(Util.formatURI(SharedData.comment_info_template,
                        guest ? "游客" : SharedData.sqlManager.user.getUsername(c.user),
                        guest,
                        c.date,
                        c.content,
                        c.id,
                        c.article,
                        c.censor)).append(',');
            }
            if (sb.charAt(sb.length() - 1) == ',')
                sb.deleteCharAt(sb.length() - 1);
        }
        return sb.append("]}").toString();
    }
    @PostMapping("/comment/fetch")
    public String getCommentContent(@RequestParam("id")int id){
        Comment comment=(Comment)SharedData.sqlManager.comment.getItem(CommentDao.ID,Integer.toString(id));
        if(comment==null)
            return Util.formatURI(SharedData.operation_fail,"Comment not exist");
        if(!ControllerUtil.hasEditPerm(comment.user)&&comment.censor!=Comment.CENSOR_PASS)
            return Util.formatURI(SharedData.operation_fail,"Comment not censored");
        boolean guest=comment.user==Comment.GUEST_USER;
        String username="游客";
        if(!guest){
            MyUserDetails author=(MyUserDetails)SharedData.sqlManager.user.getItem(UserDao.ID,Integer.toString(comment.user));
            username=author.getUsername();
        }
        return "{\"state\":\"success\",\"comment\":"+Util.formatURI(SharedData.comment_info_template,
                username,
                guest,
                comment.date,
                comment.content,
                comment.id,
                comment.article,
                comment.censor)+"}";
    }
    @PostMapping("/comment/uncensored")
    public String getUncensoredList(){
        return "{\"state\":\"success\",\"list\":"+ Arrays.toString(SharedData.sqlManager.comment.getUncensoredList())+"}";
    }
    @PostMapping("/comment/censor")
    public String censorComment(@RequestParam("id")int id,
                                @RequestParam("censor")int censor){
        if(censor<0||censor>2)
            return Util.formatURI(SharedData.operation_fail,"Invalid censor value");
        Comment comment=(Comment)SharedData.sqlManager.comment.getItem(CommentDao.ID,Integer.toString(id));
        if(comment==null)
            return Util.formatURI(SharedData.operation_fail,"Comment not exist");
        comment.censor=censor;
        SharedData.sqlManager.comment.updateItem(CommentDao.ID,Integer.toString(comment.id),comment);
        return SharedData.operation_success_no_information;
    }
}
