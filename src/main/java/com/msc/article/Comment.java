package com.msc.article;

import com.msc.SharedData;

import java.util.Calendar;

public class Comment{
    public int id;
    public final int article;
    public final int user;
    public String content;
    public String date;
    public int censor=NOT_CENSORED;

    public static final int NOT_CENSORED=0;
    public static final int CENSOR_FAIL=1;
    public static final int CENSOR_PASS=2;

    public static final int GUEST_USER=-1;

    public Comment(int article,int user,String content){
        this.article=article;
        this.user=user;
        this.content=content;
        date= SharedData.dateFormat.format(Calendar.getInstance().getTime());
    }
}
