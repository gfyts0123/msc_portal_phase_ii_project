package com.msc.security;

import com.msc.SharedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Spring Security配置。
 * */
@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/user/updateinfo","/user/uploadavatar","/user/getcurrentuser","/user/changepass",
                        "/article/new","/article/modify","/article/delete",
                        "/comment/edit","/comment/delete","/article/image/upload","/article/image/delete").authenticated()
                .antMatchers("/article/uncensored","/article/censor",
                        "/comment/uncensored","/comment/censor").hasAuthority("ROLE_ADMIN")
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(new LoginSuccessHandler())
                .failureHandler(new LoginFailureHandler())
                .permitAll()
                .and()
                .logout()
                .logoutSuccessHandler(new LogoutHandler())
                .permitAll();
    }
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return SharedData.service;
    }
    @Autowired
    public void configure(AuthenticationManagerBuilder builder)throws Exception{
        builder.userDetailsService(SharedData.service)
                .passwordEncoder(new BCryptPasswordEncoder());
    }
}
