package com.msc.security;

import com.msc.SharedData;
import com.msc.util.Util;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException {
        httpServletResponse.setStatus(403);
        httpServletResponse.setContentType("application/json;charset=utf-8");
        String result;
        if(e instanceof UsernameNotFoundException)
            result= Util.formatURI(SharedData.operation_fail,"User not exist");
        else if(e instanceof BadCredentialsException)
            result=Util.formatURI(SharedData.operation_fail,"Wrong password");
        else
            result=Util.formatURI(SharedData.operation_fail,"Login fail");
        e.printStackTrace();
        httpServletResponse.getWriter().append(result);
    }
}
