package com.msc;

import com.msc.sql.SqlManager;
import com.msc.user.MailManager;
import com.msc.user.MyUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;

public class SharedData {
    private static final Logger logger= LoggerFactory.getLogger(SharedData.class);
    public static SqlManager sqlManager;
    public static MyUserDetailsService service=new MyUserDetailsService();
    public static SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static ScriptEngine js;
    public static MailManager mail;

    public static final String operation_success_no_information="{\"state\":\"success\"}";
    public static final String operation_fail="{\"state\":\"error\",\"message\":\"%s\"}";
    public static final String user_info_template="{\"username\":\"%s\",\"id\":%d,\"gender\":%d,\"isAdmin\":%s,\"desc\":\"%s\",\"email\":\"%s\"}";
    public static final String article_info_template="{\"title\":\"%s\",\"content\":\"%s\",\"views\":%d,\"likes\":%d,\"dislikes\":%d,\"author\":\"%s\",\"id\":%d,\"express\":%d,\"date\":\"%s\",\"censor\":%d}";
    public static final String article_prompt_template="{\"title\":\"%s\",\"views\":%d,\"likes\":%d,\"dislikes\":%d,\"author\":\"%s\",\"id\":%d,\"date\":\"%s\",\"censor\":%d,\"comments\":%d}";
    public static final String comment_info_template="{\"username\":\"%s\",\"guest\":%s,\"date\":\"%s\",\"content\":\"%s\",\"id\":%d,\"article\":%d,\"censor\":%d}";

    public static BufferedImage defaultAvatar;
    public static BufferedImage picture404;

    public static void initializeData(){
        defaultAvatar=new BufferedImage(100,100,BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g=(Graphics2D)defaultAvatar.getGraphics();
        g.setColor(Color.black);
        g.fillRect(0,0,100,100);
        g.setColor(Color.gray);
        g.fillRect(20,50,60,50);
        g.setColor(Color.lightGray);
        g.fillOval(40,30,20,20);

        picture404 = new BufferedImage(100,100,BufferedImage.TYPE_3BYTE_BGR);
        g=(Graphics2D)picture404.getGraphics();
        g.setColor(Color.black);
        g.fillRect(0,0,100,100);
        g.setColor(Color.red);
        g.fillPolygon(new int[]{0,10,50,90,100,60,100,90,50,10,0,40,0},new int[]{10,0,40,0,10,50,90,100,60,100,90,50,10},12);

        mail=new MailManager("mscportal@qq.com","jonrloapqsufegai");

        sqlManager=new SqlManager("jdbc:mysql://localhost:3306/mscportal?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC","root","mscpad*rgby");
        sqlManager.initialize();

        try{
            js=new ScriptEngineManager().getEngineByExtension("js");
        }catch (Exception e){
            logger.error("Failed to get javascript script engine",e);
        }
    }
}
