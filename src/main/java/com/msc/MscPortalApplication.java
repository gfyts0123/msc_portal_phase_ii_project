package com.msc;

import com.msc.user.MyUserDetails;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

/**
 * 应用程序入口。
 * */
@SpringBootApplication
public class MscPortalApplication {

    public static void main(String[] args) {
        SharedData.initializeData();
        SpringApplication.run(MscPortalApplication.class, args);
        Thread t=new Thread(() -> {
            Scanner s=new Scanner(System.in);
            while(true){
                if(s.next().equalsIgnoreCase("regadmin")){
                    String username=s.next();
                    String password=s.next();
                    MyUserDetails user=new MyUserDetails(username,password,"not set",MyUserDetails.ADMIN,false);
                    SharedData.sqlManager.user.addItem(user);
                    System.out.println("注册成功，用户名:"+username);
                }else{
                    System.out.println("输入\"regadmin <用户名> <密码>\"来注册一个管理员账户");
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

}
