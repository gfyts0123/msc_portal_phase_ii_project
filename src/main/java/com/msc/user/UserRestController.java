package com.msc.user;

import com.msc.SharedData;
import com.msc.sql.UserDao;
import com.msc.util.ControllerUtil;
import com.msc.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

@RestController
public class UserRestController {
    private static final Logger logger= LoggerFactory.getLogger(UserRestController.class);
    @PostMapping("/register")
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("email") String email){
        if(username.equalsIgnoreCase(""))
            return Util.formatURI(SharedData.operation_fail,"No username provided.");
        if(username.length()>20)
            return Util.formatURI(SharedData.operation_fail,"Username too long.");
        if(password.length()<8)
            return Util.formatURI(SharedData.operation_fail,"Password too short.");
        if(password.length()>72)
            return Util.formatURI(SharedData.operation_fail,"Password too long.");
        if(email.length()>50)
            return Util.formatURI(SharedData.operation_fail,"Email too long.");
        if(!email.matches(".+@.+"))
            return Util.formatURI(SharedData.operation_fail,"Invalid email address.");
        if(SharedData.sqlManager.user.getItem(UserDao.USERNAME,username)!=null)
            return Util.formatURI("Username already exists.");
        MyUserDetails user=new MyUserDetails(username,password,email,MyUserDetails.NOT_ADMIN,false);
        SharedData.sqlManager.user.addItem(user);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/user/updateinfo")
    public String updateInfo(@RequestParam("desc")String description,
                             @RequestParam("gender")int gender,
                             @RequestParam("email")String email){
        MyUserDetails user=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(gender!=0&&gender!=1)
            return Util.formatURI(SharedData.operation_fail, "Invalid gender value");
        if(description.length()>200)
            return Util.formatURI(SharedData.operation_fail,"Description too long");
        if(email.length()>50)
            return Util.formatURI(SharedData.operation_fail,"Email too long.");
        if(!email.matches(".+@.+"))
            return Util.formatURI(SharedData.operation_fail,"Invalid email address.");
        user.description=description;
        user.gender=gender;
        user.email=email;
        SharedData.sqlManager.user.updateItem(UserDao.USERNAME,user.getUsername(),user);
        ControllerUtil.refreshPrincipal(user);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/user/uploadavatar")
    public String uploadAvatar(@RequestParam("avatar") MultipartFile avatar){
        MyUserDetails user=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(avatar.getSize()>3*1024*1024)
            return Util.formatURI(SharedData.operation_fail,"Image too large");
        try {
            user.avatar= ImageIO.read(avatar.getInputStream());
            ByteArrayOutputStream baos=new ByteArrayOutputStream();
            ImageIO.write(user.avatar,"png",baos);
            SharedData.sqlManager.user.updateItem(UserDao.USERNAME,user.getUsername(),user);
            ControllerUtil.refreshPrincipal(user);
            return SharedData.operation_success_no_information;
        }catch (Exception e){
            return Util.formatURI(SharedData.operation_fail,"Failed to parse image");
        }
    }
    @PostMapping("/user/getinfo")
    public String getInfo(@RequestParam("username")String username){
        MyUserDetails user=(MyUserDetails) SharedData.sqlManager.user.getItem(UserDao.USERNAME,username);
        if(user==null)
            return Util.formatURI(SharedData.operation_fail,"User not exist");
        return "{\"state\":\"success\",\"info\":"+Util.formatURI(
                SharedData.user_info_template,
                user.getUsername(),
                user.id,
                user.gender,
                user.getAdmin()==MyUserDetails.ADMIN,
                user.description,
                user.email)+"}";
    }
    @GetMapping("/user/getavatar")
    public byte[] getAvatar(@RequestParam("username")String username){
        MyUserDetails user=(MyUserDetails) SharedData.sqlManager.user.getItem(UserDao.USERNAME,username);
        BufferedImage target;
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        try {
            if(user==null)
                target=SharedData.picture404;
            else {
                if (user.avatar == null)
                    target = SharedData.defaultAvatar;
                else
                    target = user.avatar;
            }
            ImageIO.write(target,"png",bos);
            return bos.toByteArray();
        }catch (Exception e){
            logger.error("Error while sending avatar file.");
            return Util.formatURI(SharedData.operation_fail,"Internal Server Error").getBytes();
        }
    }
    @PostMapping("/user/getcurrentuser")
    public String getCurrentUser(){
        String username= SecurityContextHolder.getContext().getAuthentication().getName();
        return Util.formatURI("{\"state\":\"success\",\"username\":\"%s\"}",username);
    }
    @PostMapping("/user/sendmail")
    public String sendVerificationMail(@RequestParam("username")String username){
        MyUserDetails user=(MyUserDetails)SharedData.sqlManager.user.getItem(UserDao.USERNAME,username);
        if(user==null)
            return Util.formatURI(SharedData.operation_fail,"User not exist");
        if(!SharedData.mail.canSendVerifyMail(user.id))
            return Util.formatURI(SharedData.operation_fail,"Operation too frequent");
        SharedData.mail.sendVerifyMail(user.id,user.email);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/user/resetpass")
    public String resetPassword(@RequestParam("username")String username,
                                @RequestParam("code")String code,
                                @RequestParam("password")String password){
        MyUserDetails user=(MyUserDetails)SharedData.sqlManager.user.getItem(UserDao.USERNAME,username);
        if(user==null)
            return Util.formatURI(SharedData.operation_fail,"User not exist");
        if(password.length()<8)
            return Util.formatURI(SharedData.operation_fail,"Password too short");
        if(password.length()>72)
            return Util.formatURI(SharedData.operation_fail,"Password too long");
        if(!SharedData.mail.verifyMailContent(user.id,code))
            return Util.formatURI(SharedData.operation_fail,"Wrong/Expired code");
        user.changePassword(password);
        SharedData.sqlManager.user.updateItem(UserDao.USERNAME,user.getUsername(),user);
        ControllerUtil.refreshPrincipal(user);
        return SharedData.operation_success_no_information;
    }
    @PostMapping("/user/changepass")
    public String changePassword(@RequestParam("old")String old,
                                 @RequestParam("new")String New){
        MyUserDetails user=(MyUserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(user==null)
            return Util.formatURI(SharedData.operation_fail,"User not exist");
        if(!user.verifyPassword(old))
            return Util.formatURI(SharedData.operation_fail,"Wrong password");
        if(New.length()<8)
            return Util.formatURI(SharedData.operation_fail,"Password too short");
        if(New.length()>72)
            return Util.formatURI(SharedData.operation_fail,"Password too long");
        user.changePassword(New);
        SharedData.sqlManager.user.updateItem(UserDao.USERNAME,user.getUsername(),user);
        ControllerUtil.refreshPrincipal(user);
        return SharedData.operation_success_no_information;
    }
}
