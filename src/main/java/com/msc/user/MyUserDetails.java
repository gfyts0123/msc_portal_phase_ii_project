package com.msc.user;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;

public class MyUserDetails implements UserDetails {
    private final String username;
    private String password;
    private final int admin;

    public String email;
    public BufferedImage avatar=null;
    public int gender=UNSPECIFIED;
    public int id;
    public String description="";

    public static final int UNSPECIFIED=0;
    public static final int MALE=1;
    public static final int FEMALE=2;
    public static final int ADMIN=1;
    public static final int NOT_ADMIN=0;


    private final transient BCryptPasswordEncoder encoder;

    public MyUserDetails(String username,String password,String email,int admin,boolean isPasswordEncoded){
        encoder=new BCryptPasswordEncoder();
        this.username=username;
        if(isPasswordEncoded)
            this.password=password;
        else
            this.password=encoder.encode(password);
        this.email=email;
        this.admin=admin;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<SimpleGrantedAuthority> authorities=new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        if(admin==ADMIN)
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        return authorities;
    }

    public boolean verifyPassword(String rawPassword){
        return encoder.matches(rawPassword,password);
    }

    public void changePassword(String newPassword){
        password=encoder.encode(newPassword);
    }

    public int getAdmin(){
        return admin;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
