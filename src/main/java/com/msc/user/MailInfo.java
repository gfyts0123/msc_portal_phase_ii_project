package com.msc.user;

public class MailInfo {
    public final long sendTime;
    public final int userId;
    public final String code;
    public final String codeCharacters="23456789ABCDEFGHJKLMNPQRSTUVWXYZ";
    public MailInfo(int userId){
        sendTime=System.currentTimeMillis();
        this.userId=userId;
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<6;i++)
            sb.append(codeCharacters.charAt((int)(Math.random()*codeCharacters.length())));
        code=sb.toString();
    }
}
