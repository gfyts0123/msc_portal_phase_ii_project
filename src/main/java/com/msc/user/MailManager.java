package com.msc.user;

import com.sun.mail.util.MailSSLSocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

public class MailManager {
    private final String senderAddr;
    private final String authentication;
    private final Properties properties;
    private final Logger logger= LoggerFactory.getLogger(MailManager.class);
    private HashMap<Integer,MailInfo> mailRecord=new HashMap<>();
    public MailManager(String senderAddr,String authentication){
        this.senderAddr=senderAddr;
        this.authentication=authentication;
        properties = new Properties();
        properties.setProperty("mail.host","smtp.qq.com");
        properties.setProperty("mail.transport.protocol","smtp");
        properties.setProperty("mail.smtp.auth","true");
        properties.put("mail.smtp.ssl.enable", "true");
        try {
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            properties.put("mail.smtp.ssl.socketFactory", sf);
        }catch (Exception e){
            logger.error("Failed to instantiate mail ssl socket factory",e);
        }
    }
    public synchronized void sendMail(String recipient,String subject,String contentHTML){
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderAddr,authentication);
            }
        });
        session.setDebug(true);
        try {
            Transport transport = session.getTransport();
            transport.connect("smtp.qq.com", senderAddr, authentication);
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(senderAddr));
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            mimeMessage.setSubject(subject);
            mimeMessage.setContent(contentHTML, "text/html;charset=UTF-8");
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            transport.close();
        } catch (Exception e){
            logger.error("Failed to send mail",e);
        }
    }
    public boolean canSendVerifyMail(int userId){
        if(mailRecord.containsKey(userId))
            return System.currentTimeMillis()-mailRecord.get(userId).sendTime>60*1000;
        clean();
        return true;
    }
    public void sendVerifyMail(int userId,String mailAddr){
        MailInfo info=new MailInfo(userId);
        mailRecord.put(userId,info);
        sendMail(mailAddr,"微软学生俱乐部门户网站","<h2>微软学生俱乐部门户网站密码找回</h2>" +
                "<br />您的验证码为:<font color='red'>"+info.code+"</font>" +
                "<br />验证码有效期:5分钟。" +
                "<br />注意：不要将您的验证码泄露给其他人，这可能会导致您的账户丢失。" +
                "<br />如果这不是您本人的操作，请忽略此封邮件。");
    }
    public boolean verifyMailContent(int userId,String code){
        clean();
        if(!mailRecord.containsKey(userId))
            return false;
        return mailRecord.get(userId).code.equalsIgnoreCase(code);
    }
    private void clean(){
        ArrayList<Integer> expired=new ArrayList<>();
        for(int i:mailRecord.keySet()){
            if(System.currentTimeMillis()-mailRecord.get(i).sendTime>5*60*1000)
                expired.add(i);
        }
        for(int i:expired)
            mailRecord.remove(i);
    }
}
