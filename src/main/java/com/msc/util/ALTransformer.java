package com.msc.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * A util class for transforming list to/from array.
 * */
public class ALTransformer{
    public static int[] toIntArray(ArrayList<Integer> list){
        int[] result=new int[list.size()];
        Iterator<Integer> it=list.iterator();
        int counter=0;
        while(it.hasNext())
            result[counter++]=it.next();
        return result;
    }
    public static ArrayList<Integer> toIntegerList(int[] arr){
        ArrayList<Integer> result=new ArrayList<>();
        for(int a:arr)
            result.add(a);
        return result;
    }
}
