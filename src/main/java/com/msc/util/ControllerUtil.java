package com.msc.util;

import com.msc.user.MyUserDetails;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;

public class ControllerUtil {
    public static boolean isAuthenticated(){
        return SecurityContextHolder.getContext().getAuthentication()!=null&&
                SecurityContextHolder.getContext().getAuthentication().isAuthenticated()&&
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);
    }
    public static boolean hasEditPerm(int authorId){
        if(!isAuthenticated())
            return false;
        else {
            MyUserDetails current=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return current.id==authorId || current.getAdmin() == MyUserDetails.ADMIN;
        }
    }
    public static boolean isAdmin(){
        if(!isAuthenticated()){
            return false;
        }else{
            MyUserDetails current=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return current.getAdmin() == MyUserDetails.ADMIN;
        }
    }
    public static String getClientIP(HttpServletRequest req){
        String ip = "unknown";
        String ipAddresses = req.getHeader("X-Forwarded-For");
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses))
            ipAddresses = req.getHeader("Proxy-Client-IP");
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses))
            ipAddresses = req.getHeader("HTTP_CLIENT_IP");
        if (ipAddresses == null || ipAddresses.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses))
            ipAddresses = req.getHeader("X-Real-IP");
        if (ipAddresses != null && ipAddresses.length() != 0)
            ip = ipAddresses.split(",")[0];
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ipAddresses))
            ip = req.getRemoteAddr();
        return ip;
    }
    public static void refreshPrincipal(MyUserDetails user){
        Authentication auth=SecurityContextHolder.getContext().getAuthentication();
        UsernamePasswordAuthenticationToken token=new UsernamePasswordAuthenticationToken(user,auth.getCredentials(),auth.getAuthorities());
        token.setDetails(SecurityContextHolder.getContext().getAuthentication().getDetails());
        SecurityContextHolder.getContext().setAuthentication(token);
    }
    public static int getCurrentUserId(){
        if(isAuthenticated()){
            MyUserDetails current=(MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return current.id;
        }else
            return -1;
    }
}
