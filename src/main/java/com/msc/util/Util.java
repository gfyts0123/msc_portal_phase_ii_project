package com.msc.util;

import com.msc.SharedData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Util {
    private static Logger logger= LoggerFactory.getLogger(Util.class);
    public static String formatURI(String format,Object... args){
        try {
            for (int i = 0; i < args.length; i++) {
                if(args[i] instanceof String) {
                    String line = "encodeURI(\"" + args[i].toString().replaceAll("\"", "\\\\\"").replaceAll("\r", "\\\\\\r").replaceAll("\n", "\\\\\\n") + "\");";
                    args[i] = SharedData.js.eval(line);
                }
            }
        }catch (Exception e){
            logger.error("Failed to format URI.",e);
        }
        return String.format(format, args);
    }
    public static String formatWithoutQuote(String format,Object... args){
        for(int i=0;i<args.length;i++)
            if(args[i] instanceof String)
                args[i]=args[i].toString().replaceAll("'","\\\\'");
        return String.format(format, args);
    }
}
