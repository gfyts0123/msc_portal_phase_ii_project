# 用户相关API

> 注: 所有的请求URL和返回内容都可调整。  
> 提示: 如果非表单POST请求返回403，请确认请求数据中是否携带<kbd>_csrf</kbd>项目。  
> 注: 需要登陆之后才可调用的接口以<kbd>\*</kbd>标识  
> 注: 所有POST和GET的参数都应经过**URLEncode**编码，以避免其值中含有特殊字符。  
> 注: 所有服务端返回的**json文件**中的值都经过**encodeURI**编码以避免出现特殊字符。

## 用户数据(非API)
每个用户包含如下数据:  
| 描述 | 类型 | 最大长度(*表示可更改) |
| ---- | ---- | ---- |
| 用户ID | Integer | 4 byte |
| 用户名 | String | 20字符* |
| 密码 | String | 72字符(BCrypt算法限制) |
| 邮箱 | String | 50字符* |
| 个人介绍 | String | 200字符* |
| 性别 | Integer\* | 1 byte |
| 是否为管理员 | Integer | 1 byte |
| 头像 | Image | 16 MB |
| 帖子列表 | Blob | 64 KB |  

\*: 性别由以下三个整数表示:  
0: 未指定(保密)  
1: 男性  
2: 女性  
> 注: 超出最大长度会导致数据库操作出错。

## 用户登录 **(Spring Security 内置)**
URL:<kbd>/login.html</kbd>  
请求方式:<kbd>POST</kbd>   
请求参数:  
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |
| password | 密码 |  

请求示例:
```text
username=Smith&password=9822e18en
```
请求成功返回:  
```json
{"status":"success"}
```

请求失败返回:
> 用户名不存在
> ```json
> {"status":"error","message":"User%20not%20exist"}
> ```

> 密码错误
> ```json
> {"status":"error","message":"Wrong%20password"}
> ```

> 其他错误
> ```json
> {"status":"error","message":"Login%20fail"}
> ```

## \*用户注销 **(Spring Security 内置)**
URL:<kbd>/logout</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:无  
请求成功返回:
> 跳转至<kbd>login.html?logout</kbd>

## 用户注册
URL:<kbd>/register</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:  
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |
| password | 密码 |
| email | 邮箱 |  

请求示例:
```text
username=LiHua&password=englishExaminor&email=english%40composition.com
```

请求失败返回:
| 失败原因 | 返回json的message参数 |
| ---- | ---- |
| 用户名重复 | Username already exists |
| 用户名大于20字符 | Username too long |
| 用户名为空 | No username provided |
| 密码小于8位 | Password too short |
| 密码大于72字符 | Password to long |
| 邮件格式错误\* | Invalid email address |
| 邮件字符串过长 | Email too long |

\*: 正确的邮件应符合以下正则表达式:
```regex
.+@.+
```
> 注: 如果可以，在表单发送之前自检一下这些错误。

请求成功返回:
```json
{"state":"success"}
```

## \*更新当前用户信息
URL:<kbd>/user/updateinfo</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:  
| 名称 | 说明 |
| ---- | ---- |
| desc | 个人介绍 |
| gender | 性别 |
| email | 邮箱 |

请求示例:
```text
desc=%E6%88%91%E6%98%AF%E6%9D%8E%E5%8D%8E%EF%BC%8Ca+student+from+XinHua%E4%B8%AD%E5%AD%A6%E3%80%82&gender=1&email=10364192%40qq.com
```


请求失败返回:  
| 失败原因 | 返回json的message参数 |
| ---- | ---- |
| 个人描述超出200字符 | Description too long |
| 性别数据不正确 | Invalid gender value |
| 邮件格式错误 | Invalid email address |
| 邮件字符串过长 | Email too long |

请求成功返回: 
```json
{"state":"success"}
```

## \*获取当前登录的用户名
URL:<kbd>/user/getcurrentuser</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:无  
请求成功返回示例:  
```json
{"state":"success","username":"Li%20Hua"}
```

> 注: 通过检查返回json的state参数，你可以快速了解请求是否成功。

## 获取指定用户信息
URL: <kbd>/user/getinfo</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |

请求示例:
```text
username=LiHua
```

请求成功返回示例:
```json
{"state":"success","info":{"username":"Li%20Hua","id":23,"gender":1,"isAdmin":false,"desc":"%E6%88%91%E6%98%AF%E6%9D%8E%E5%8D%8E%EF%BC%8Ca%20student%20from%20XinHua%E4%B8%AD%E5%AD%A6%E3%80%82","email":"english@composition.com"}}
```

请求失败返回示例:
> 错误: 用户不存在
> ```json
> {"state":"error","message":"User%20not%20exist"}
> ```

## \*更新当前用户头像
URL: <kbd>/user/uploadavatar</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| avatar | 新的头像 |

> 注: 注意大小限制，Spring本身有对上传文件的大小限制(默认1 MB，已更改为10 MB)。  
> 提醒: 表单属性中请添加`enctype="multipart/form-data"`以便传输文件。

请求成功返回:
```json
{"state":"success"}
```
  
请求失败返回:
| 失败原因 | 返回json的message参数 |
| ---- | ---- |
| 图片超出3MB | Image too large |
| 图片解析失败 | Failed to parse image |

## 获取指定用户头像
URL: <kbd>/user/getavatar</kbd>  
请求方式:<kbd>GET</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |

请求示例:
```html
<img src="/user/getavatar?username=Li+Hua" />
```

> 注意: 地址请使用**URLEncode**编码

请求成功返回:
> 头像文件二进制数据  

请求失败返回:
| 失败原因 | 返回数据 |
| ---- | ---- |
| 用户不存在 | *待定* |

## \*更换密码
URL: <kbd>/user/changepass</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| old | 旧密码 |
| new | 新密码 |

请求示例:
```text
old=12ex9m0h1e&new=239gqs2sm9
```

请求成功返回: 
```json
{"state":"success"}
```

请求失败返回:
| 错误原因 | 返回内容 |
| ---- | ---- |
| 用户不存在 | User not exist |
| 旧密码错误 | Wrong password |
| 新密码小于8位 | Password too short |
| 新密码大于72字符 | Password too long |

## 重置密码
URL: <kbd>/user/resetpass</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |
| code | 邮箱验证码 |
| password | 新密码 |

请求示例:
```text
username=Li+Hua&code=8EB73G&new=239gqs2sm9
```

请求成功返回: 
```json
{"state":"success"}
```

请求失败返回:
| 错误原因 | 返回json的message参数 |
| ---- | ---- |
| 用户不存在 | User not exist |
| 验证码错误 | Wrong/Expired code |
| 新密码小于8位 | Password too short |
| 新密码大于72字符 | Password too long |

## 发送验证邮件
URL: <kbd>/user/sendmail</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |

请求示例:
```text
username=Li+Hua
```

请求成功返回示例: 
```json
{"state":"success"}
```

请求失败返回示例:
> 用户不存在
> ```json
> {"state":"error","message":"User%20not%20exist"}
> ```

> 操作过于频繁（两次请求间隔小于1分钟）
> ```json
> {"state":"error","message":"Operation%20too%20frequent"}
> ```