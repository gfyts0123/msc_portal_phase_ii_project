# Frontend Todo List

### 说明
`resources/static`文件夹中为静态资源，无需配置映射即可访问。  
`resources/templates`文件夹中的页面为模板，需要后端手动配置映射才可访问，适用于例如`bilibili.com/video/BV23QM02ZM879G2Q`这样的带有任意路径的网页。 

### TODO
+ 修改`resources/static`中的主页`index.html`。  
+ 修改`resources/static`中的`register.html`。
+ 在`resources/static`中创建`error.html`以代替默认的错误页面。  
+ 修改`resources/static`中的`login.html`，但不要更改表单提交逻辑。  
+ 添加用户信息编辑/查看页面。

# Backend Todo List

*Add Item*