# 文章相关API

> 注: 所有的请求URL和返回内容都可调整。  
> 提示: 如果非表单POST请求返回403，请确认请求数据中是否携带<kbd>_csrf</kbd>项目。  
> 注: 需要登陆之后才可调用的接口以<kbd>\*</kbd>标识  
> 注: 所有POST和GET的参数都应经过**URLEncode**编码，以避免其值中含有特殊字符。  
> 注: 所有服务端返回的**json文件**中的**值**都经过**encodeURI**编码以避免出现特殊字符。  

|  | 目录 | |
| ---- | ---- | ---- |
| 1.文章数据(非API) | 12.\*删除评论 |
| 2.评论数据(非API) | 13.\*编辑评论 |
| 3.\*当前用户新建文章 | 14.获取评论列表 |
| 4.\*修改文章 | 15.获取评论内容 |
| 5.获取指定用户的文章列表 | 16.\*获取未审核文章列表(Admin) |
| 6.获取文章 | 17.\*获取未审核评论列表(Admin) |
| 7.\*删除文章 | 18.\*更改文章审核状态(Admin) |
| 8.点赞/踩文章 | 19.\*更改评论审核状态(Admin) |
| 9.评论文章 | 20.\*上传图片 |
| 10.获取最新文章 | 21.获取图片 |
| 11.搜索文章 | 22.\*删除图片 |

## 1.文章数据(非API)
每篇文章包含如下数据:
| 描述 | 类型 | 最大长度(*表示可更改) |
| ---- | ---- | ---- |
| 文章ID | Integer | 4 byte |
| 标题 | String | 50字符* |
| 作者ID | Integer | 4 byte |
| 内容 | String | 64 KB |
| 创建日期 | DateTime | 8 byte |
| 点赞用户列表 | Blob | 64 KB |
| 点赞游客IP列表 | Blob | 64 KB |
| 点踩用户列表 | Blob | 64 KB |
| 点踩游客IP列表 | Blob | 64 KB |
| 评论列表 | Blob | 64 KB |
| 浏览数 | Integer | 4 byte |
| 审核状态\* | Integer | 1 byte |

> 注: 超出最大长度会导致数据库操作出错。  

\*:审核状态由以下数字表示，在所有地方通用。  
0: 未审核  
1: 审核不通过  
2: 审核通过

## 2.评论数据(非API)
| 描述 | 类型 | 最大长度(*表示可更改) |
| ---- | ---- | ---- |
| 评论ID | Integer | 4 byte |
| 评论时间 | DateTime | 8 byte |
| 评论用户ID | Integer | 4 byte |
| 评论内容 | String | 64 KB |
| 归属文章ID | Integer | 4 byte |
| 审核状态 | Integer | 1 byte |

## 3.\*当前用户新建文章
URL:<kbd>/article/new</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| title | 文章标题 |
| content | 文章内容 |  

请求示例:
```text
title=Java%3A+%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F&content=%E7%AC%AC%E4%B8%80%E6%AD%A5%3A+%E4%B8%8B%E8%BD%BD%E5%B9%B6%E5%AE%89%E8%A3%85Java+.+.+.+%E5%A4%B1%E8%B4%A5%E3%80%82%0D%0A%E7%BB%93%E6%9D%9F%E2%88%9A
```
请求成功返回:
```json
{"state":"success"}
```

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 标题为空 | No title provided |
| 标题超出50字符 | Title too long |
| 内容过长 | Content too long |

## 4.\*修改文章
URL:<kbd>/article/modify</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| title | 文章标题 |
| content | 文章内容 |
| id | 要修改的文章原ID |

请求示例:
```text
title=Java%3A+%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F&content=%E7%AC%AC%E4%B8%80%E6%AD%A5%3A+%E4%B8%8B%E8%BD%BD%E5%B9%B6%E5%AE%89%E8%A3%85Java+.+.+.+%E5%A4%B1%E8%B4%A5%E3%80%82%0D%0A%E7%BB%93%E6%9D%9F%E2%88%9A&id=23
```

> 注: 修改过的文章会重新标记为"未经审核"。  

> 注: 未审核/审核未通过的文章也可以编辑。

请求成功返回:
```json
{"state":"success"}
```

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| ID所指文章不属于当前用户且当前用户不是管理员 | Access denied |
| 文章不存在 | Article not exist |
| 标题为空 | No title provided |
| 标题超出50字符 | Title too long |
| 内容过长 | Content too long |

## 5.获取指定用户的文章列表
URL:<kbd>/article/getlist</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| username | 用户名 |
| off | 列表区间开始值(可选) |
| length | 列表区间长度(可选) |

> 注: 若没有length参数(len参数为0)，将返回如下的值，且不会出错:  
> ```json
> {"state":"success","listlength":21,"list":[]}
> ```
> 可用此方法查询文章列表长度。

请求示例:
```text
username=Li+Hua&off=5&len=2
```

> 注: 示例中返回以**创建时间降序排列**的文章列表中的第5和第6篇文章。  
> 文章编号从0开始，最大为(文章列表长度-1)。

> 注: 若当前用户既不是指定用户，也不是管理员，审核未通过的文章将不会出现在列表中，即返回的列表中会跳过这些文章。

请求成功返回示例:  
```json
{"state":"success","listlength":21,"list":[{"title":"Java:%20%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F","views":213,"likes":5,"dislikes":72,"author":"Li%20Hua","id":17,"date":"2020-01-03%2012:23","censor":2,"comments":14},{"title":"java.lang.OutofMemoryError%E5%87%BA%E7%8E%B0%E7%9A%84%E5%8E%9F%E5%9B%A0","views":453,"likes":98,"dislikes":2,"author":"Li%20Hua","id":19,"date":"2019-12-14%2023:14","censor":2,"comments":22}]}
```
部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| listlength | 文章列表总长度 |
| list | 文章概要列表 |
| comments | 评论数 |
| 其余参见*6.获取文章*| |

> 注: 若当前用户既不是指定用户，也不是管理员，文章列表总长度将不包括未通过审核的文章。

请求失败返回:
> 用户不存在  
> ```json
> {"state":"error","message":"User%20not%20exist"}
> ```

> 指定的列表区间超出列表长度  
> ```json
> {"state":"error","message":"Index%20out%20of%20bounds"}
> ```

## 6.获取文章
URL:<kbd>/article/fetch</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 文章ID |

请求示例:
```text
id=23
```
请求成功返回示例:  
```json
{"state":"success","article":{"title":"Java:%20%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F","content":"%E7%AC%AC%E4%B8%80%E6%AD%A5:%20%E4%B8%8B%E8%BD%BD%E5%B9%B6%E5%AE%89%E8%A3%85Java%20.%20.%20.%E5%A4%B1%E8%B4%A5%0D%0A%E7%BB%93%E6%9D%9F%E2%88%9A","views":213,"likes":5,"dislikes":72,"author":"Li%20Hua","id":17,"express":1,"date":"2020-01-03%2012:23","censor":2}}
```

部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| views | 浏览数 |
| likes | 赞数 |
| dislikes | 踩数 |
| author | 作者用户名 |
| comments | 评论ID |
| id | 文章ID |
| express\* | 当前用户对此文章的赞/踩状态 |
| censor | 审核状态 |

\*: 状态说明  
0: 既没有点赞也没有点踩  
1: 点过赞  
2: 点过踩

> 注: 每一次成功请求都将增加此文章的浏览数。

> 注: 如果文章的评论列表中有评论未审核/审核未通过，且当前用户既不是其作者也不是管理员，则该评论不会出现在评论列表中。

请求失败返回:
> 文章不存在  
> ```json
> {"state":"error","message":"Article%20not%20exist"}
> ```

> 文章未审核或审核未通过，且当前用户既不是管理员也不是文章作者。  
> ```json
> {"state":"error","message":"Article%20not%20censored"}
> ```

## 7.\*删除文章
URL:<kbd>/article/delete</kbd>
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 要删除的文章ID |

请求示例:
```text
id=23
```
请求成功返回:
 ```json
 {"state":"success"}
 ```

> 注: 未审核/审核未通过的文章也可以删除。

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| ID所指文章不属于当前用户且当前用户不是管理员 | Access denied |
| 文章不存在 | Article not exist |

## 8.点赞/踩文章
URL:<kbd>/article/express</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 要点赞/踩的文章ID |
| action\* | 要执行的动作 |

\*: 以下值为合法动作  
0: 取消点赞/踩  
1: 点赞  
2: 踩

请求示例:
```text
id=23&action=1
```
请求成功返回示例:
```json
{"state":"success"}
```
请求失败返回:
> 文章不存在  
> ```json
> {"state":"error","message":"Article%20not%20exist"}
> ```

> 文章未审核或审核不通过
>> 注: 这种情况下即使是作者和管理员也不能进行点赞/踩操作。  
> ```json
> {"state":"error","message":"Article%20not%20censored"}
> ```


> 已经点赞/踩过了
>> 注: 此种情况只在连续多次点赞/踩时出现，先点赞后再踩，踩会覆盖点赞的效果，不会出现此错误，反之亦然。
> ```json
> {"state":"error","message":"Already%20like/disliked"}
> ```

> 还没有点赞/踩就进行取消
> ```json
> {"state":"error","message":"Not%20like/disliked"}
> ```

> action值不合法
> ```json
> {"state":"error","message":"Invalid%20action"}
> ```

## 9.评论文章
URL:<kbd>/comment/new</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 要评论的文章ID |
| content | 评论内容 |

> 注: id为-1时将评论添加到对网站的评论中。

请求示例:
```text
id=23&content=233%F0%9F%98%82%E7%AC%91%E6%AD%BB
```
请求成功返回:
```json
{"state":"success"}
```

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 文章不存在 | Article not exist |
| 文章未审核或审核不通过 | Article not censored |

## 10.获取最新文章
URL:<kbd>/article/feed</kbd>
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| off | (可选)列表起始位置 |
| length | (可选)列表长度 |

请求示例:
```text
off=0&length=2
```
请求成功返回:
 ```json
{"state":"success","listlength":102,"list":[{"title":"Java:%20%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F","views":213,"likes":5,"dislikes":72,"author":"Li%20Hua","id":17,"date":"2020-01-03%2012:23","censor":2,"comments":14},{"title":"java.lang.OutofMemoryError%E5%87%BA%E7%8E%B0%E7%9A%84%E5%8E%9F%E5%9B%A0","views":453,"likes":98,"dislikes":2,"author":"Li%20Hua","id":19,"date":"2019-12-14%2023:14","censor":2,"comments":22}]}
 ```

> 注: 未审核/审核未通过的文章永远不会出现在此处列表中。

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| length>20 | Length too large |
| 请求的区间超出列表长度 | Index out of bounds |

## 11.\*搜索文章
URL:<kbd>/article/search</kbd>
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| query | 关键词 |
| off | (可选)列表起始位置 |
| length | (可选)列表长度 |

请求示例:
```text
query=java&off=0&length=0
```
请求成功返回:
 ```json
{"state":"success","listlength":2,"list":[{"title":"Java:%20%E4%BB%8E%E5%85%A5%E5%9D%91%E5%88%B0%E5%85%A5%E5%9C%9F","views":213,"likes":5,"dislikes":72,"author":"Li%20Hua","id":17,"date":"2020-01-03%2012:23","censor":2,"comments":14},{"title":"java.lang.OutofMemoryError%E5%87%BA%E7%8E%B0%E7%9A%84%E5%8E%9F%E5%9B%A0","views":453,"likes":98,"dislikes":2,"author":"Li%20Hua","id":19,"date":"2019-12-14%2023:14","censor":2,"comments":22}]}
 ```

> 注: 未审核/审核未通过的文章永远不会出现在此处列表中。

> 注: **重要**当文章标题包含完整关键词时，该文章才会出现在结果列表中。

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| length>20 | Length too large |
| 请求的区间超出列表长度 | Index out of bounds |

## 12.\*删除评论

URL:<kbd>/comment/delete</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 评论ID |

请求示例:
```text
id=23
```
请求成功返回:
```json
{"state":"success"}
```

> 注: 如果当前用户是评论作者或管理员，即使目标评论未审核/审核未通过也可以删除。  

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 当前用户既不是目标评论作者也不是管理员 | Access denied |
| 评论不存在 | Comment not exist |

## 13.\*编辑评论

URL:<kbd>/comment/edit</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 评论ID |
| content | 评论内容 |

请求示例:
```text
id=23&content=233%F0%9F%98%82%E7%AC%91%E6%AD%BB
```
请求成功返回:
```json
{"state":"success"}
```

> 注: 如果当前用户是评论作者或管理员，即使目标评论未审核/审核未通过也可以进行修改。  

> 注: 修改过的评论会重新标记为"未经审核"。   

请求失败返回:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 当前用户既不是目标评论作者也不是管理员 | Access denied |
| 评论不存在 | Comment not exist |

## 14.获取文章评论列表
URL:<kbd>/comment/getlist</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 文章ID |
| off | 列表区间开始值(可选) |
| length | 列表区间长度(可选) |

> 注: id为-1时返回网站评论列表。  

> 注: 若没有length参数(len参数为0)，将返回如下的值，且不会出错:  
> ```json
> {"state":"success","listlength":21,"list":[]}
> ```
> 可用此方法查询评论列表长度。

请求示例:
```text
id=23&off=5&len=2
```

请求成功返回:
```json
{"state":"success","list":[{"username":"Li%20Hua","guest":false,"date":"2020-01-03%2012:23","content":"233%F0%9F%98%82%E7%AC%91%E6%AD%BB","id":152,"article":33,"censor":2},{"username":"%E6%B8%B8%E5%AE%A2","guest":true,"date":"2020-01-01%2015:22","content":"???","id":173,"article":33,"censor":2}]}
```

> 注: 示例中返回以**创建时间降序排列**的评论列表中的第5和第6个评论。  
> 评论编号从0开始，最大为(评论列表长度-1)。

> 注: 如果当前用户既不是评论作者也不是管理员，则未经审核的评论ID不会出现在列表中。

部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| list | 评论ID列表 |
| guest | 指明发表评论的用户是否为游客 |

请求失败返回:
> 文章不存在。
> ```json
> {"state":"error","message":"Article%20not%20exist"}
> ```

> 文章未审核。
> ```json
> {"state":"error","message":"Article%20not%20censored"}
> ```

> 请求区间越界。
> ```json
> {"state":"error","message":"Index%20out%20of%20bounds"}
> ```

## 15.获取评论内容
URL:<kbd>/comment/fetch</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 评论ID |

请求示例:
```text
id=23
```
请求成功返回:
```json
{"state":"success","comment":{"username":"Li%20Hua","guest":false,"date":"2020-01-03%2012:23","content":"233%F0%9F%98%82%E7%AC%91%E6%AD%BB","id":152,"article":33,"censor":2}}
```

部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| id | 评论ID |
| article | 评论所属文章ID |
| censor | 审核状态 |

请求失败返回:
> 评论不存在。
> ```json
> {"state":"error","message":"Article%20not%20censored"}
> ```



## 16.\*获取未审核文章列表(Admin)
URL:<kbd>/article/uncensored</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:无

请求成功返回示例:
```json
{"state":"success","list":[62,64,65]}
```

部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| list | 文章ID列表 |

## 17.\*获取未审核评论列表(Admin)
URL:<kbd>/comment/uncensored</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:无

请求成功返回示例:
```json
{"state":"success","list":[102,105,106]}
```

部分参数说明:
| 名称 | 说明 |
| ---- | ---- |
| list | 评论ID列表 |

## 18.\*更改文章审核状态(Admin)
URL:<kbd>/article/censor</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 文章ID |
| censor | 要将文章的审核状态更改为的值 |

请求示例:
```text
id=64&censor=2
```

请求成功返回示例:
```json
{"state":"success"}
```

请求失败返回示例:
> 文章不存在  
> ```json
> {"state":"error","message":"Article%20not%20exist"}
> ```

> censor值不合法
> ```json
> {"state":"error","message":"Invalid%20censor%20value"}
> ```

## 19.\*更改评论审核状态(Admin)
URL:<kbd>/comment/censor</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 文章ID |
| censor | 要将文章的审核状态更改为的值 |

请求示例:
```text
id=235&censor=1
```

请求成功返回示例:
```json
{"state":"success"}
```

请求失败返回示例:
> 评论不存在。
> ```json
> {"state":"error","message":"Comment%20not%20found"}
> ```

> censor值不合法
> ```json
> {"state":"error","message":"Invalid%20censor%20value"}
> ```

## 20.\*上传图片
URL:<kbd>/article/image/upload</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| image | 图片数据(MultipartFile) |

请求成功返回示例:
```json
{"state":"success","id":33}
```
> 注: 返回的id在获取图片时会用到。

请求失败返回示例:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 图片超出3MB | Image too large |
| 图片解析失败 | Failed to parse image |

## 21.获取图片
URL:<kbd>/article/image</kbd>  
请求方式:<kbd>GET</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 图片ID |

请求示例:
```html
<img src="/article/image?id=33" />
```

请求成功返回: 图片数据

## 20.\*上传图片
URL:<kbd>/article/image/delete</kbd>  
请求方式:<kbd>POST</kbd>  
请求参数:
| 名称 | 说明 |
| ---- | ---- |
| id | 图片ID |

请求成功返回示例:
```json
{"state":"success"}
```

请求失败返回示例:
| 失败原因 | 返回内容 |
| ---- | ---- |
| 图片不是当前用户上传的且当前用户不是管理员 | Access denied |
| 图片不存在 | Picture not exist |